package com.example.timudal;

import com.example.timudal.model.Common;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Collections;

@SpringBootApplication
public class TimudalApplication {

    public static Common common = new Common();

    public static void main(String[] args) {
        SpringApplication.run(TimudalApplication.class, args);

    }

}
