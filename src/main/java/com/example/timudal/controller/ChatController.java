package com.example.timudal.controller;

import com.example.timudal.model.ChatMessage;
import com.example.timudal.model.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.stereotype.Controller;

import javax.websocket.OnOpen;
import javax.websocket.Session;

@Controller
public class ChatController {

    private static final Logger logger = LoggerFactory.getLogger(ChatController.class);

    @MessageMapping("/chat.sendMessage")
    @SendTo("/topic/public")
    public Message<ChatMessage> sendMessage(@Payload Message<ChatMessage> chatMessage) {
        return chatMessage;
    }

    @MessageMapping("/chat.addUser")
    @SendTo("/topic/public")
    public Message<ChatMessage> addUser(@Payload Message<ChatMessage> chatMessage, SimpMessageHeaderAccessor headerAccessor){
        headerAccessor.getSessionAttributes().put("username", chatMessage.getData().getSender());
        return chatMessage;
    }

    @OnOpen
    public void onOpen(Session session){
        logger.info("Open seeion "+ session.getId());
    }

}
