package com.example.timudal.model;

import lombok.Data;

@Data
public class Message<T> {


    private String messageType;

    private T data;
}
