package com.example.timudal.model;

public enum MessageType {
    CHAT,
    JOIN,
    LEAVE
}
